package com.example.TodoList;

import com.example.TodoList.entity.TodoItem;
import com.example.TodoList.exception.ItemNotFoundException;
import com.example.TodoList.repository.TodoItemRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoListIntegrationTest {
    @Autowired
    private MockMvc client;

    @Autowired
    private TodoItemRepository todoItemRepository;

    @BeforeEach
    void setUp(){
        todoItemRepository.deleteAll();
    }
    
    @Test
    void should_return_all_items_when_get_all_todo_items_given_() throws Exception {
        TodoItem todoItem1 = getTodoItem1();
        todoItemRepository.save(todoItem1);
        client.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todoItem1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todoItem1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todoItem1.getDone()));
    }

    @Test
    void should_modify_item_text_when_edit_item_text_given_id_and_item_with_text() throws Exception {
        TodoItem previousItem = getTodoItem1();
        todoItemRepository.save(previousItem);

        TodoItem todoItemUpdate = new TodoItem(null, "text update", null);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedItemJson = objectMapper.writeValueAsString(todoItemUpdate);
        client.perform(put("/todos/{id}", previousItem.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updatedItemJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        TodoItem afterUpdateItem = todoItemRepository.findById(previousItem.getId()).orElseThrow(ItemNotFoundException::new);;
        Assertions.assertEquals(previousItem.getId(), afterUpdateItem.getId());
        Assertions.assertEquals(todoItemUpdate.getText(), afterUpdateItem.getText());
    }

    @Test
    void should_modify_item_done_when_toggle_item_given_id_and_item_with_done() throws Exception {
        TodoItem previousItem = getTodoItem1();
        todoItemRepository.save(previousItem);

        TodoItem todoItemUpdate = new TodoItem(null, null, false);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedItemJson = objectMapper.writeValueAsString(todoItemUpdate);
        client.perform(put("/todos/{id}", previousItem.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedItemJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        TodoItem afterUpdateItem = todoItemRepository.findById(previousItem.getId()).orElseThrow(ItemNotFoundException::new);;
        Assertions.assertEquals(previousItem.getId(), afterUpdateItem.getId());
        Assertions.assertEquals(true, afterUpdateItem.getDone());
    }

    @Test
    void should_return_item_when_get_item_given_id() throws Exception {
        //given
        TodoItem todoItem = getTodoItem1();
        todoItemRepository.save(todoItem);

        //when
        client.perform(get("/todos/{id}", todoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoItem.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoItem.getDone()));
    }

    @Test
    void should_add_item_when_create_item_given_item() throws Exception {
        TodoItem todoItem2 = getTodoItem2();

        ObjectMapper objectMapper = new ObjectMapper();
        String itemJson = objectMapper.writeValueAsString(todoItem2);

        client.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(itemJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoItem2.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoItem2.getDone()));
    }

    @Test
    void should_delete_item_when_delete_given_item_id() throws Exception {
        TodoItem todoItem = getTodoItem3();
        todoItemRepository.save(todoItem);

        client.perform(delete("/todos/{id}", todoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));

        Assertions.assertFalse(todoItemRepository.findById(todoItem.getId()).isPresent());
    }

    private static TodoItem getTodoItem1(){
        TodoItem todoItem = new TodoItem();
        todoItem.setText("text 1");
        todoItem.setDone(false);
        return todoItem;
    }

    private static TodoItem getTodoItem2(){
        TodoItem todoItem = new TodoItem();
        todoItem.setText("text 2");
        todoItem.setDone(false);
        return todoItem;
    }

    private static TodoItem getTodoItem3(){
        TodoItem todoItem = new TodoItem();
        todoItem.setText("text 3");
        todoItem.setDone(false);
        return todoItem;
    }
}
