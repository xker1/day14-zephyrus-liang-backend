create table if not exists todos (

    id         bigint auto_increment primary key,

    text       varchar(512) null,

    done     BOOLEAN     null,

);