package com.example.TodoList.service;

import com.example.TodoList.exception.ItemNotFoundException;
import com.example.TodoList.entity.TodoItem;
import com.example.TodoList.repository.TodoItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoItemService {
    private final TodoItemRepository todoItemRepository;

    public TodoItemService(TodoItemRepository todoItemRepository) {
        this.todoItemRepository = todoItemRepository;
    }

    public List<TodoItem> getAllTodoItems(){
        return todoItemRepository.findAll();
    }

    public void update(Long id, TodoItem updateItem) {
        TodoItem itemById = getTodoItem(id);
        if (updateItem.getText() != null) {
            itemById.setText(updateItem.getText());
        }
        if (updateItem.getDone() != null){
            itemById.setDone(!itemById.getDone());
        }
        todoItemRepository.save(itemById);
    }

    public TodoItem getTodoItem(Long id) {
        return todoItemRepository.findById(id).orElseThrow(ItemNotFoundException::new);
    }

    public TodoItem createTodoItem(TodoItem todoItem) {
        return todoItemRepository.save(todoItem);
    }

    public void deleteTodoItem(Long id) {
        todoItemRepository.deleteById(id);
    }
}
