package com.example.TodoList.controller;

import com.example.TodoList.entity.TodoItem;
import com.example.TodoList.service.TodoItemService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/todos")
@RestController
//@CrossOrigin()
public class TodoItemController {
    private final TodoItemService todoItemService;

    public TodoItemController(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @GetMapping()
    public List<TodoItem> getAllTodoItems(){
        return todoItemService.getAllTodoItems();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateItem(@PathVariable Long id, @RequestBody TodoItem updateItem){
        todoItemService.update(id, updateItem);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TodoItem getTodoItem(@PathVariable Long id){
        return todoItemService.getTodoItem(id);
    }


    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public TodoItem createTodoItem(@RequestBody TodoItem todoItem){
        return todoItemService.createTodoItem(todoItem);
    }

    @DeleteMapping("/{id}")
    public void deleteTodoItem(@PathVariable Long id){
        todoItemService.deleteTodoItem(id);
    }
}
