package com.example.TodoList.exception;

public class ItemNotFoundException extends RuntimeException{
    public ItemNotFoundException() {
        super("Item Not Found!");
    }
}
